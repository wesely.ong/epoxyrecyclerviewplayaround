package com.example.epoxyexperiment.main_act

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.epoxyexperiment.R
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val controller = HomeEpoxyController()
        Log.d("mainActivity", "start")
        epoxyRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@HomeActivity)
            setControllerAndBuildModels(controller)
        }
    }
}

