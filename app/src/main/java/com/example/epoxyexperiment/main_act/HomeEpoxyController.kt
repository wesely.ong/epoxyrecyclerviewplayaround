package com.example.epoxyexperiment.main_act

import android.util.Log
import android.widget.Toast
import com.airbnb.epoxy.CarouselModelBuilder
import com.airbnb.epoxy.CarouselModel_
import com.airbnb.epoxy.EpoxyController
import com.airbnb.epoxy.EpoxyModel
import com.example.epoxyexperiment.epoxy.ClickableModel_
import com.example.epoxyexperiment.epoxy.HeaderModel_
import com.example.epoxyexperiment.epoxy.NormalModel_
import com.example.epoxyexperiment.model.Lv1Image
import com.example.epoxyexperiment.model.Lv2Image
import com.example.epoxyexperiment.model.Lv3Image

class HomeEpoxyController : EpoxyController() {

    override fun buildModels() {
        Log.d("controller", "buildModels")
        var id = 0

        HeaderModel_()
            .text("NormalModel+Lv1Image")
            .size(50f)
            .id(id++) // must have
            .addTo(this) // must have

        (1..3).map { Lv1Image(it) }.forEach {
            Log.d("controller", "buildModels/forEach")
            NormalModel_()
                .imgRes(it.imageRes)
                .text(it.text)
                .id(id++) // must have
                .addTo(this) // must have
        }

        HeaderModel_()
            .text("ClickableModel + Lv2, On Screen = 2.0f")
            .size(15f)
            .id(id++)
            .addTo(this)

        carousel {
            id("carousel")
            numViewsToShowOnScreen(2f)
            withModelsFrom((1..50).map { Lv2Image(it) }) {
                ClickableModel_()
                    .clickListener { view -> Toast.makeText(view.context, it.text, Toast.LENGTH_SHORT).show() }
                    .imgRes(it.imageRes)
                    .text(it.text)
                    .id(id++)
            }
        }

        HeaderModel_()
            .text("Normal Lv3, OnScreen=3.5/padding=20/height=160dp")
            .size(15f)
            .id(id++)
            .addTo(this)

        carousel {
            id("carousel")
            numViewsToShowOnScreen(3.5f)
            this.onBind { model, view, position -> }
            this.onUnbind { model, view -> }
            this.paddingDp(20)
            withModelsFrom((1..50).map { Lv3Image(it) }) {
                NormalModel_()
                    .imgRes(it.imageRes)
                    .text(it.text)
                    .id(id++)
            }
        }


        HeaderModel_()
            .text("Show On Screen = 5f")
            .size(15f)
            .id(id++)
            .addTo(this)

        carousel {
            id("carousel")
            numViewsToShowOnScreen(5f)
            withModelsFrom((1..50).map { Lv1Image(it) }) {
                ClickableModel_()
                    .clickListener { view -> Toast.makeText(view.context, it.text, Toast.LENGTH_SHORT).show() }
                    .imgRes(it.imageRes)
                    .text(it.text)
                    .id(id++)
            }
        }

        HeaderModel_()
            .text("Header Text in Controller")
            .size(50f)
            .id(id++) // must have
            .addTo(this) // must have

        (1..100).map { Lv3Image(it) }.forEach {
            Log.d("controller", "buildModels/forEach")
            NormalModel_()
                .imgRes(it.imageRes)
                .text(it.text)
                .id(id++) // must have
                .addTo(this) // must have
        }

        HeaderModel_()
            .text("Footer 3")
            .size(20f)
            .id(id++)
            .addTo(this)

        carousel {
            id("carousel")
            numViewsToShowOnScreen(5f)
            withModelsFrom((1..50).map { Lv2Image(it) }) {
                ClickableModel_()
                    .clickListener { view -> Toast.makeText(view.context, it.text, Toast.LENGTH_SHORT).show() }
                    .imgRes(it.imageRes)
                    .text(it.text)
                    .id(id++)
            }
        }

        HeaderModel_()
            .text("Clickable + Lv2")
            .size(20f)
            .id(id++)
            .addTo(this)

        carousel {
            id("carousel")
            numViewsToShowOnScreen(8f)
            withModelsFrom((1..50).map { Lv2Image(it) }) {
                ClickableModel_()
                    .clickListener { view -> Toast.makeText(view.context, it.text, Toast.LENGTH_SHORT).show() }
                    .imgRes(it.imageRes)
                    .text(it.text)
                    .id(id++)
            }
        }

        HeaderModel_()
            .text("Footer 1")
            .size(20f)
            .id(id++)
            .addTo(this)

    }

    /** For use in the buildModels method of EpoxyController. A shortcut for creating a Carousel model, initializing it, and adding it to the controller.
     *
     */
    inline fun EpoxyController.carousel(modelInitializer: CarouselModelBuilder.() -> Unit) {
        CarouselModel_().apply {
            modelInitializer()
        }.addTo(this)
    }

    /** Add models to a CarouselModel_ by transforming a list of items into EpoxyModels.
     *
     * @param items The items to transform to models
     * @param modelBuilder A function that take an item and returns a new EpoxyModel for that item.
     */
    inline fun <T> CarouselModelBuilder.withModelsFrom(
        items: List<T>,
        modelBuilder: (T) -> EpoxyModel<*>
    ) {
        models(items.map { modelBuilder(it) })
    }
}