package com.example.epoxyexperiment.model

class Lv3Image(position: Int) {
    val imageRes = arrayListOf(
        android.R.color.holo_blue_bright,
        android.R.color.holo_blue_dark,
        android.R.color.holo_blue_light
    )[position % 3]

    val text = "Lv3 #$position"
}