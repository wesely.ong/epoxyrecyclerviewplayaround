package com.example.epoxyexperiment.model

class Lv1Image(position: Int) {
    val imageRes = arrayListOf(
        android.R.color.darker_gray,
        android.R.color.holo_green_light,
        android.R.color.holo_green_dark
    )[position % 3]

    val text = "Lv1 #$position"
}