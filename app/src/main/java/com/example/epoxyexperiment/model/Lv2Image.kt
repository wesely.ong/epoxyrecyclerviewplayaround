package com.example.epoxyexperiment.model

class Lv2Image(position: Int) {
    val imageRes = arrayListOf(
        android.R.color.holo_purple,
        android.R.color.holo_red_dark,
        android.R.color.holo_red_light
    )[position % 3]

    val text = "Lv2 #$position"
}