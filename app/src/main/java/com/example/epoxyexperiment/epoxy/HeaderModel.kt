package com.example.epoxyexperiment.epoxy

import android.util.Log
import android.view.View
import com.airbnb.epoxy.EpoxyModelWithHolder
import android.widget.TextView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.example.epoxyexperiment.R
import kotlinx.android.synthetic.main.listitem_header.view.*


@EpoxyModelClass(layout = R.layout.listitem_header)
abstract class HeaderModel : EpoxyModelWithHolder<HeaderModel.HeaderHolder>() {
    @EpoxyAttribute
    var text: String = "Template Lv1"
    @EpoxyAttribute
    var size: Float = 15f

    override fun bind(holder: HeaderHolder) {
        Log.d("NormalModel", "binding:$text")
        holder.tv.text = text
        holder.tv.textSize = size
    }

    inner class HeaderHolder : EpoxyHolder() {
        lateinit var tv: TextView

        override fun bindView(itemView: View) {
            tv = itemView.tvHeader
        }
    }
}