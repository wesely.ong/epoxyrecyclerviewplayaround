package com.example.epoxyexperiment.epoxy

import android.util.Log
import android.view.View
import android.widget.ImageView
import com.airbnb.epoxy.EpoxyModelWithHolder
import android.widget.TextView
import androidx.annotation.DrawableRes
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyHolder
import com.airbnb.epoxy.EpoxyModelClass
import com.example.epoxyexperiment.R
import kotlinx.android.synthetic.main.listitem_lv2.view.*


@EpoxyModelClass(layout = R.layout.listitem_lv2)
abstract class ClickableModel : EpoxyModelWithHolder<ClickableModel.Lv1ViewHolder>() {

    @EpoxyAttribute
    var text: String = "Template"

    @EpoxyAttribute
    @DrawableRes
    var imgRes: Int = R.mipmap.ic_launcher

    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    var clickListener: View.OnClickListener? = null

    override fun bind(holder: Lv1ViewHolder) {
        Log.d("NormalModel", "binding:$text")
        holder.tv.text = text
        holder.iv.setImageResource(imgRes)
        holder.iv.setOnClickListener(clickListener)
    }

    override fun unbind(holder: Lv1ViewHolder) {
        super.unbind(holder)
        holder.iv.setOnClickListener(null)
    }

    inner class Lv1ViewHolder : EpoxyHolder() {
        lateinit var tv: TextView
        lateinit var iv: ImageView

        override fun bindView(itemView: View) {
            tv = itemView.tvLv2
            iv = itemView.ivLv2
        }
    }
}